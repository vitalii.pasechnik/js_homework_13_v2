const timerLayout = document.body.querySelector('.layout-timer');

let playback;

export default function timer(action, count = 3) {
    let delay = count;
    if (action === 'start') {
        timerLayout.textContent = `${delay--}`;
        timerLayout.classList.add('active');
        playback = setInterval(() => {
            timerLayout.textContent = `${delay--}`;
            if (delay < 0) {
                timerLayout.textContent = `${count}`;
                delay = count - 1;
            }
        }, 1000);

    } else if (action === 'stop') {
        timerLayout.textContent = '';
        timerLayout.classList.remove('active');
        clearInterval(playback);
    }
}