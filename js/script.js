"use strict";
import timer from "./timer.js";

// const images = document.body.querySelector('.images-wrapper');
// const nextBtn = document.body.querySelector('.btn-next');
// nextBtn.addEventListener('click', () => showNext());

// const showNext = () => {
//     // console.log([...images.children].findIndex(el => el.classList.contains('active')));
//     const activeInd = [...images.children].findIndex(el => el.classList.contains('active'));
//     const imgArr = [...images.children];
//     const nextImg = imgArr[activeInd + 1] || imgArr[0];
//     imgArr[activeInd].classList.remove('active');
//     nextImg.classList.add('active');
// } 

const images = document.body.querySelectorAll('.img');
const nextBtn = document.body.querySelector('.btn-next');
const prevBtn = document.body.querySelector('.btn-prev');
const startBtn = document.body.querySelector('.btn-start');
const pauseBtn = document.body.querySelector('.btn-pause');

nextBtn.addEventListener('click', () => showNext());
prevBtn.addEventListener('click', () => showPrev());
startBtn.addEventListener('click', () => autoPreview('start'));
pauseBtn.addEventListener('click', () => autoPreview('stop'));

const deltaPx = 420;
const maxDeltaPx = (images.length - 1) * -deltaPx;
const changeLeftPosition = (value) => images.forEach(img => img.style.left = `${+img.style.left.slice(0, -2) + value}px`);
disableEl(prevBtn);

function showPrev() {
    setActiveEl(nextBtn);
    changeLeftPosition(deltaPx);
    images[0].style.left.slice(0, -2) === '0' && disableEl(prevBtn);
}

function showNext() {
    setActiveEl(prevBtn);
    changeLeftPosition(-deltaPx);
    images[0].style.left.slice(0, -2) === `${maxDeltaPx}` && disableEl(nextBtn);
};

function disableEl(...nodes) {
    nodes.forEach(el => {
        el.classList.add('disabled');
        el.setAttribute('disabled', true);
    })
};

function setActiveEl(...nodes) {
    nodes.forEach(el => {
        el.classList.remove('disabled');
        el.removeAttribute('disabled');
    })
};

let move;
function autoPreview(action) {
    if (action === 'start') {
        timer('start');
        setActiveEl(pauseBtn);
        disableEl(nextBtn, prevBtn, startBtn);
        let moveRight = true;
        move = setInterval(() => {
            if (images[0].style.left.slice(0, -2) > maxDeltaPx && moveRight) {
                changeLeftPosition(-deltaPx);
            } else if (images[0].style.left.slice(0, -2) < 0) {
                changeLeftPosition(deltaPx);
                moveRight = false;
            } else {
                changeLeftPosition(-deltaPx);
                moveRight = true;
            }
        }, 3000);
    } else if (action === 'stop') {
        clearInterval(move);
        if (+images[0].style.left.slice(0, -2) === 0) {
            setActiveEl(nextBtn, startBtn);
        } else if (+images[0].style.left.slice(0, -2) === maxDeltaPx) {
            setActiveEl(prevBtn, startBtn);
        } else {
            setActiveEl(prevBtn, nextBtn, startBtn);
        }
        disableEl(pauseBtn);
        timer('stop');
    };
}